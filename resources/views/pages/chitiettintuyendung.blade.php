@extends('layout.index')
@section('title')
Chi tiết tuyển dụng
@endsection
@section('content')
<div class="content">
	<div class="detail-header">
		<div class="background">
		
		</div>
		<div class="container information_company">
			<div class="thongtin">
				<div class="col-md-2 img-logo">
					<img src="images/10409501.png" alt="img"/>
				</div>
				<div class="col-md-8 thongtin-congty">
					<h3>Chuyên Viên Quản Lý Hoạt Động Đào Tạo</h3>
					<p>Công ty cổ phần peopleone - Hà Nội</p>
					<p>Mức lương</p>
					<p>1278 lượt xem - hết hạn trong 24 ngày</p>
				</div>
				<div class="col-md-2 btn_nopdon">
					<a href="#">Nộp Đơn</a>
				</div>
			</div>		
		</div>	
	</div>
	<div class="container detail-content">
		<ul class="nav nav-tabs three-tabs">
			<li><a href="#">Thông Tin</a></li>
			<li><a href="#">Công Ty</a></li>
			<li><a href="#">Việc làm khác từ công ty</a></li>
		</ul>
	<div class="container content">
		<div class="col-md-8 left-content">
			<h3>CÁC PHÚC LỢI DÀNH CHO BẠN</h3>
			<p>Lương theo năng lực + Thưởng nóng theo dự án và hiệu quả công việc</p>
			<p>Cơ hội huấn luyện: trở thành quản lý phát triển phần mềm với dự án và lãnh đạo có kinh nghiệm làm</p>
			<p>Thưởng các ngày lễ trong nămg</p>
			<h3>Mô tả công việc</h3>
			<p>** Vai trò, chức năng</p>
			<span>1. Người lập kế hoạch, xây dựng lịch chi tiết</span>
			<span>2. Người lập kế hoạch, xây dựng lịch chi tiết</span>
			<span>3. Người lập kế hoạch, xây dựng lịch chi tiết</span>
			<span>4. Người lập kế hoạch, xây dựng lịch chi tiết</span>
			<span>5. Người lập kế hoạch, xây dựng lịch chi tiết</span>
			<p>** Nhiệm vụ</p>
			<span>1. Lập kế hoạch, lịch tổ chức thực hiện các dự án đào tạo cho doanh nghiệp</span>
			<span>2. Lập kế hoạch, lịch tổ chức thực hiện các dự án đào tạo cho doanh nghiệp</span>

		</div>
		<div class="col-md-4 right-content">
			<div class="thongtin-dangtuyen">
				<div>
					<i class="fa fa-calendar"></i>
					<p>
						<span>NGÀY ĐĂNG TUYỂN</span>
						<span>11/05/2018</span>
					</p>
				</div>
				<div>	
					<i class="fa fa-briefcase"></i>
					<p>
						<span>CẤP BẬC</span>
						<span>Nhân Viên</span> 
					</p>
				</div>
				<div>	
					<i class="fa fa-user-md"></i>
					<p>
						<span>NGÀNH NGHỀ</span>
						<span>Hành chánh/Thư ký, Giáo dục/ Đào tạo, nhân sự</span>
					</p>
				</div>
				<div>	
					<i class="fa fa-flag"></i>
					<p>
						<span>KỸ NĂNG</span>
						<span>Giáo Dục Đào Tạo, Hành Chính - Nhân Sự, Quản Lý Giáo Dục</span>					
					</p>
				</div>
				<div>	
					<i class="fa fa-language"></i>
					<p>
						<span>NGÔN NGỮ TRÌNH BÀY HỒ SƠ</span>
						<span>Bất kỳ</span>					
					</p>	
				</div>
			</div>
		</div>
	</div>
	<div class="detail-footer">
		<h3>Việc làm bạn sẽ yêu thích</h3>
		<div class="col-md-4 information">
			<div class="abc">
				<a href="#"><img src="images/10444475.png" alt="img" /></a>
				<div>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
				</div>				
				
			</div>
		</div>
		<div class="col-md-4 information">
			<div class="abc">
				<a href="#"><img src="images/10444475.png" alt="img" /></a>
				<div>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
				</div>				
				
			</div>
		</div>
		<div class="col-md-4 information">
			<div class="abc">
				<a href="#"><img src="images/10444475.png" alt="img" /></a>
				<div>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
				</div>				
				
			</div>
		</div>
		<div class="col-md-4 information">
			<div class="abc">
				<a href="#"><img src="images/10444475.png" alt="img" /></a>
				<div>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
				</div>				
				
			</div>
		</div>
		<div class="col-md-4 information">
			<div class="abc">
				<a href="#"><img src="images/10444475.png" alt="img" /></a>
				<div>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
				</div>				
				
			</div>
		</div>
		<div class="col-md-4 information">
			<div class="abc">
				<a href="#"><img src="images/10444475.png" alt="img" /></a>
				<div>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
					<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
				</div>				
				
			</div>
		</div>																				

	</div>
	</div>

</div>
@endsection