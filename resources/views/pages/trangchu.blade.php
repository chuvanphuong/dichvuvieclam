@extends('layout.index')
@section('title')
Trang Chủ
@endsection
@section('content')
	<div class="slider">
		<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="100000">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#myCarousel" data-slide-to="1"></li>
		    <li data-target="#myCarousel" data-slide-to="2"></li>
		    <li data-target="#myCarousel" data-slide-to="3"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		      	<img src="images/slide1.jpg" alt="Los Angeles">
				<div class="carousel-caption">
					<h3>New York</h3>
					<p>We love the Big Apple!</p>
				</div>
		    </div>

		    <div class="item">
		      	<img src="images/slide2.jpg" alt="Chicago">
		      	<div class="carousel-caption">
					<h3>New York</h3>
					<p>We love the Big Apple!</p>
				</div>
		    </div>

		    <div class="item">
		      	<img src="images/slide3.jpg" alt="New York">
		      	<div class="carousel-caption">
					<h3>New York</h3>
					<p>We love the Big Apple!</p>
				</div>
		    </div>
		    <div class="item">
		      	<img src="images/slide5.jpg" alt="New York">
		      	<div class="carousel-caption">
					<h3>New York</h3>
					<p>We love the Big Apple!</p>
				</div>
		    </div>		    
		  </div>
		</div>
		<div class="container search-container">
			<div class="search-widget">
				<h1>Tìm Công Việc Mơ Ước. <strong>Nâng Bước Thành Công!</strong></h1>
				<div class="form-search">
					<form action="" method="Post">
						<input type="text" class="form-control search-all" placeholder="Nhập chức danh, vị trí, kỹ năng...">
						<select class="form-control cate-search">
						  <option value="volvo">Tất cả ngành nghề</option>
						  <option value="saab">Saab</option>
						  <option value="vw">VW</option>
						</select>
						<select class="form-control vitri">
						  <option value="volvo">Hà Nội</option>
						  <option value="saab">Saab</option>
						  <option value="vw">VW</option>
						</select>
						<button type="submit" class="btn btn-default btn-search">Tìm Việc</button>
					</form>
				</div>
				<p><a href="#">Đăng tải hồ sơ của bạn</a></p>
			</div>
		</div>		
	</div>
	<!-- !end_slider -->
	<div class="container logo-caccongty">
		<h1>Các Công Ty Hàng Đầu</h1>
		<div class="row">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 logo_congty">
				<a href="#">
					<div id="logo-box">
						<img src="images/panasonic.jpg" alt="img"/>
					</div>
					<span>Panasonic VietNam group</span>
				</a>
			
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 logo_congty">
				<a href="#">
					<div id="logo-box">
						<img src="images/fpt.jpg" alt="img"/>
					</div>
					<span>Panasonic VietNam group</span>
				</a>
			
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 logo_congty">
				<a href="#">
					<div id="logo-box">
						<img src="images/eximbank.jpg" alt="img"/>
					</div>
					<span>Panasonic VietNam group</span>
				</a>
			
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 logo_congty">
				<a href="#">
					<div id="logo-box">
						<img src="images/gettherm.jpg" alt="img"/>
					</div>
					<span>Panasonic VietNam group</span>
				</a>
			
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 logo_congty">
				<a href="#">
					<div id="logo-box">
						<img src="images/academy.jpg" alt="img"/>
					</div>
					<span>Panasonic VietNam group</span>
				</a>
			
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 logo_congty">
				<a href="#">
					<div id="logo-box">
						<img src="images/pyco.jpg" alt="img"/>
					</div>
					<span>Panasonic VietNam group</span>
				</a>
			
			</div>
		</div>
	</div>
	<!-- !logo_caccongty -->
	<div class="container kenhvieclam">
		<h1>Các Kênh Việc Làm Của Chúng Tôi</h1>
		<div class="row">
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 box-kenhvieclam">
				<div class="img-channel">
					<img src="images/channel-tmj.png" alt="img" />
				</div>
				<div class="abc"></div>
				<div class="content">
                    <img src="images/channel-icon-tmj.png">
                    <div class="channel-label">Việc Làm Cấp Quản Lý</div>
                    <div class="number-jobs">76 Việc Làm</div>
                    <a target="_blank" href="#" class="btn btn-primary" tabindex="-1">Xem</a>
                </div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 box-kenhvieclam">
				<div class="img-channel">
					<img src="images/channel-it.png" alt="img" />
				</div>
				<div class="abc"></div>
				<div class="content">
                    <img src="images/channel-icon-it.png">
                    <div class="channel-label">Việc Làm Ngành IT</div>
                    <div class="number-jobs">1546 Công Việc</div>
                    <a target="_blank" href="#" class="btn btn-primary" tabindex="-1">Xem</a>
                </div>				
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 box-kenhvieclam">
				<div class="img-channel">
					<img src="images/channel-intern.png" alt="img" />
				</div>
				<div class="abc"></div>
				<div class="content">
                    <img src="images/channel-icon-intern.png">
                    <div class="channel-label">Mới Tốt Nghiệp</div>
                    <div class="number-jobs">611 Việc Làm</div>
                    <a target="_blank" href="" class="btn btn-primary" tabindex="-1">Xem</a>
                </div>
			</div>
		</div>
	</div>
	<!-- !end_kenhvieclam -->
	<div class="container likejob">
		<h1>Việc Làm Bạn sẽ thích</h1>
		<div class="tab-container">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-job">
				<ul>
					<li><a href="#">Việc làm tốt nhất</a></li>
					<li><a href="#">Việc làm gợi ý</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 box-listjob">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 img-listjob">
							<img src="images/10313077.png" alt="img" />
						</div>
						<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 infomationjob">
							<p><strong>Lorem ipsum dolor sit amet.</strong></p>
							<p>Lorem ipsum dolor sit amet.</p>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 hot">
							<span>Hot</span>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 box-listjob">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 img-listjob">
							<img src="images/10444475.png" alt="img" />
						</div>
						<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 infomationjob">
							<p><strong>Lorem ipsum dolor sit amet.</strong></p>
							<p>Lorem ipsum dolor sit amet.</p>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 hot">
							<span>Hot</span>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 box-listjob">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 img-listjob">
							<img src="images/10313077.png" alt="img" />
						</div>
						<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 infomationjob">
							<p><strong>Lorem ipsum dolor sit amet.</strong></p>
							<p>Lorem ipsum dolor sit amet.</p>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 hot">
							<span>Hot</span>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 box-listjob">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 img-listjob">
							<img src="images/10313077.png" alt="img" />
						</div>
						<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 infomationjob">
							<p><strong>Lorem ipsum dolor sit amet.</strong></p>
							<p>Lorem ipsum dolor sit amet.</p>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 hot">
							<span>Hot</span>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 box-listjob">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 img-listjob">
							<img src="images/10313077.png" alt="img" />
						</div>
						<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 infomationjob">
							<p><strong>Lorem ipsum dolor sit amet.</strong></p>
							<p>Lorem ipsum dolor sit amet.</p>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 hot">
							<span>Hot</span>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 box-listjob">
						<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 img-listjob">
							<img src="images/10313077.png" alt="img" />
						</div>
						<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 infomationjob">
							<p><strong>Lorem ipsum dolor sit amet.</strong></p>
							<p>Lorem ipsum dolor sit amet.</p>
						</div>
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 hot">
							<span>Hot</span>
						</div>
					</div>
				</div>																									
			</div>

		</div>
	</div>
	<!-- !end_likejobs -->
	<div class="container nhatuyendung-noibat">
		<h1>Nhà tuyển dụng nổi bật</h1>
		<div class="anhbia">
			<img src="images/1160x308_102650.jpg" alt="img" />
		</div>
		<div class="thongtin-nhatuyendung">
			<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 avatar">
				<img src="images/logo.png" alt="img" />
			</div>
			<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
				<h3>Vietnamwork</h3>
				<p><a href="#">Lorem ipsum dolor sit amet.</a></p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos tempore similique fugit alias, tenetur quod id dolorum veniam doloremque quisquam repellat temporibus, excepturi at placeat? Dolore assumenda fugiat sequi at perferendis atque alias quibusdam laborum itaque perspiciatis omnis ullam exercitationem quas repellat odio, hic! Quibusdam adipisci in ad quia praesentium.</p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 xemthem">
				 <p><a href="#">Xem thêm</a></p>
			</div>
		</div>
	</div>
	<!-- !end_nhatuyendung-noibat -->
	<div class="container nhatuyendung-hangdau">
		<h1>Các nhà tuyển dụng hàng đầu</h1>
		<div class="logo-list">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>															
		</div>
		<div class="logo-list">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 img-list-logo">
				<a href="#"><img src="images/88x43-BMS_102612.jpg" alt="img" /></a>
			</div>															
		</div>		
	</div>
	<!-- !end_cacnhatuyendung-noibat -->
	<div class="container tuvannghenghiep">
		<h1>Tư vấn nghề nghiệp với HR insider</h1>
		<div class="tuvan">
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 list-tuvan">
				<img src="images/img1.jpg" alt="img" />
				<div class="text_list">
					<h3>13 câu hỏi ấn tượng khiến nhà tuyển dụng muốn chọn bạn ngay... </h3>
					<p>Dù bạn đang tìm kiếm công việc đầu tiên trong đời hay đã là “lão làng” dày dạn kinh nghiệm, việc tạo ấn tượng tốt xuyên suốt cuộc phỏng vấn sẽ là chìa khóa giúp bạn nhận được thư... </p>
					<p><a href="#">Xem thêm</a></p>				
				</div>
			</div>
		</div>
		<div class="tuvan">
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 list-tuvan">
				<img src="images/img1.jpg" alt="img" />
				<div class="text_list">
					<h3>13 câu hỏi ấn tượng khiến nhà tuyển dụng muốn chọn bạn ngay... </h3>
					<p>Dù bạn đang tìm kiếm công việc đầu tiên trong đời hay đã là “lão làng” dày dạn kinh nghiệm, việc tạo ấn tượng tốt xuyên suốt cuộc phỏng vấn sẽ là chìa khóa giúp bạn nhận được thư... </p>
					<p><a href="#">Xem thêm</a></p>				
				</div>
			</div>
		</div>
		<div class="tuvan">
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 list-tuvan">
				<img src="images/img1.jpg" alt="img" />
				<div class="text_list">
					<h3>13 câu hỏi ấn tượng khiến nhà tuyển dụng muốn chọn bạn ngay... </h3>
					<p>Dù bạn đang tìm kiếm công việc đầu tiên trong đời hay đã là “lão làng” dày dạn kinh nghiệm, việc tạo ấn tượng tốt xuyên suốt cuộc phỏng vấn sẽ là chìa khóa giúp bạn nhận được thư... </p>
					<p><a href="#">Xem thêm</a></p>				
				</div>
			</div>
		</div>				
	</div>
	<!-- !end_tuvannghenghiep -->
@endsection	