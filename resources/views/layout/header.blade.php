	<div class="header">
			<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 logo">
				<img src="images/logo.png" alt="img">
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 list-menu">
				<ul>
					<Li><a href="#">Tất cả việc làm</a></Li>
					<Li><a href="#">Công ty</a></Li>
					<Li><a href="#">Tư vấn</a></Li>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 thongtinuser">
				<ul>
					<li style="border-left: 1px solid grey;">
						<a href="#"><span class="fa fa-bell"></span></a>
					</li>
					<li>
						<a href="#"><span class="fa fa-lg fa-comments-o"></span></a>
					</li>
					<li>
						<a href="#" data-toggle="modal" data-target="#myModal1"><span class="fa fa-fw fa-lg fa-sign-in"></span>Đăng nhập</a>
												  <!-- Modal -->
						  <div class="modal fade" id="myModal1" role="dialog">
						    <div class="modal-dialog">
						    
						      <!-- Modal content-->
						      <div class="modal-content">
						        <div class="modal-header header-login">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h3 class="modal-title">Đăng Nhập</h3>
						        </div>
						        <div class="modal-body">
						         	<a href="#">Dùng tài khoản google</a>
						         	<a href="#">Dùng tài khoản facebook</a>
						         	<div class="form-dangky">
						         		<h3>Sử dụng tài khoản vietnamwork</h3>
						         		<form action="#" method="Post" class="form-nhap">
						         			<div class="row">
						         				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 email">
						         					<label>Email</label>
						         					<input class="form-control" type="text">									
						      					</div>						         				
						         				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mật khẩu">
						         					<label>mật khẩu</label>
						         					<input class="form-control" type="passwork"> 
													<a href="#">Quyên mật khẩu?</a>
						         				</div>
						         				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 dangky">
						         					<button type="submit" class="btn btn-default">Đăng Nhập</button>
						         				</div>

						         			</div>
						         		</form>
						         	</div>
						        </div>
						        <div class="modal-footer">
						         
						        </div>
						      </div>
						      
						    </div>
						  </div>
					</li>
					<li>
						<a href="#"  data-toggle="modal" data-target="#myModal">Đăng ký</a>
						  <!-- Modal -->
						<div class="modal fade" id="myModal" role="dialog">
						    <div class="modal-dialog">
						    
							      <!-- Modal content-->
							    <div class="modal-content">
							        <div class="modal-header header-login">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h3 class="modal-title">Đăng ký thành viên</h3>
							        </div>
							        <div class="modal-body">
							         	<a href="#">Dùng tài khoản google</a>
							         	<a href="#">Dùng tài khoản facebook</a>
							         	<div class="form-dangky">
							         		<h3>Đăng ký bằng email</h3>
							         		<form action="#" method="Post" class="form-nhap">
							         			<div class="row">
							         				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 name">
							         					<label>Tên</label>
							         					<input class="form-control" type="text" placeholder="vui lòng nhập tên">
							         				</div>
							         				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 name">
							         					<label>Họ</label>
							         					<input class="form-control" type="text" placeholder="vui lòng nhập họ">
							         				</div>
							         				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 email">
							         					<label>Email</label>
							         					<input class="form-control" type="text">									
							      					</div>						         				
							         				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mật khẩu">
							         					<label>mật khẩu</label>
							         					<input class="form-control" type="passwork"> 
														<small>Mật khẩu từ 6 đến 50 ký tự, ít nhất 1 ký tự viết hoa và 1 chữ số.</small>
							         				</div>
							         				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 dangky">
							         					<button type="submit" class="btn btn-default">Đăng ký</button>
							         				</div>

							         			</div>
							         		</form>
							         	</div>
							        </div>
							        <div class="modal-footer">
							         
							        </div>
							    </div>
						      
						    </div>
						</div>
					</li>
					<li class="tuyendung">
						<a href="#">
							<span class="nha-tuyen-dung"><strong>NHÀ TUYỂN DỤNG</strong></span></br>
							<span class="dang-tin-tuyen-dung">Đăng tuyển & tìm kiếm nhân tài</span>
						</a>
					</li>
				</ul>
			</div>
	</div>
	<!-- !end_header -->