<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin_dichvuvieclam</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"><!-- dung cho dien thoai phóng to thu nhỏ màn hình -->
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- link icon -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link rel="stylesheet" href="../css/admin/style.css">		
</head>
<body>
	{{-- header --}}
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">Dịch Vụ Việc Làm</a>
	    </div>
	    <ul class="nav navbar-nav navbar-right">
	      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
	      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
	    </ul>
	  </div>
	</nav>	
	{{-- !end_header --}}
	{{-- content --}}
	<div class="container-fluid content_admin">
		<div class="row">
			<div class="col-md-3 left_content">
				<h3 class="box-title">Welcome <b>Admin</b></h3>
				<div class="box-body no-padding">
                <ul class="nav nav-pills nav-stacked">
                  <li class="active"><a href="dashboard.php"><i class="fas fa-chart-line"></i> Dashboard</a></li>
                  <li><a href="#"><i class="fas fa-briefcase"></i>Ngành Nghề</a></li>
                  <li><a href="#"><i class="fas fa-map-marker-alt"></i>Địa Điểm</a></li>
                  <li><a href="#"><i class="fas far fa-address-card" style="font-size: 10px;"></i>Tin Tuyển Dụng</a></li>
                  <li><a href="#"><i class="fas fa-angle-double-right"></i>slide</a></li>
                  <li><a href="#"><i class="fab fa-adversal" style="margin-right: 10px;"></i>Banner</a></li>
                  <li><a href="#"><i class="fas fa-user"></i>UserPersonal</a></li>
                  <li><a href="#"><i class="fas fa-user-tie"></i>UserCompany</a></li>
                  <li><a href="#"><i class="fas fa-lock"></i>Admin</a></li>
                </ul>
              </div>
			</div>
			<div class="col-md-9 right_content">
				<h3>Tin Tuyển Dụng</h3>
				<table class="table table-bordered">
					<div class="thead">
						<th>Tiêu Đề</th>
						<th>CôngTy</th>
						<th>Phúc Lợi</th>
						<th>Mô Tả</th>
						<th>Yêu Cầu</th>
						<th>Kỹ Năng</th>
						<th>Mức Lương</th>
						<th>Hạn NỘp</th>
					</div>
					<div class="tbody">
						<tr>
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>							
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>							
						<tr>
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>							
						</tr>
						<tr>
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>							
						</tr>
					</div>	
				</table>

			</div>
		</div>
		
	</div>
	{{-- !end_content --}}
	{{-- footer --}}
	<div class="container-fluid footer">
		<h4>Copyright © 2016-2017 Job Portal. All rights reserved.</h4>
	</div>
	{{-- !end-footer --}}
	
</body>
</html>