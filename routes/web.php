<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\userpersonal;
Route::get('/', function () {
    return view('welcome');
});
Route::get('trangchu', function() {
    return view('pages.trangchu');
});
Route::get('chitiet', function() {
    return view('pages.chitiettintuyendung');
});
Route::get('timkiem', function() {
    return view('pages.timkiem');
});
Route::group(['prefix' => 'admin'],function(){
	Route::get('trangchu_admin', function() {
	    return view('admin.layout.index');
	});
});
Route::get('thu', function() {
    $user = userpersonal::all();
    foreach ($user as $user) {
    	echo $user->Ten;
    }
});

