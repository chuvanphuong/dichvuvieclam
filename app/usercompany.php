<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usercompany extends Model
{
    protected $table = 'usercompany';
    public function nganhnghe(){
    	return  $this->belongsTo('App\nganhnghe','Id_NganhNghe','Id_UserCompany');
    }
    public function tintuyendung(){
    	return $this->hasMany('App\tintuyendungn','Id_Company', 'Id_TinTuyenDung','');
    }
    public function diadiem(){
    	$this->hasMany('App\diadiem','Id_DiaDiem','Id_DiaDiem');
    }
}
