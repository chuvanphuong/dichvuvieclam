<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nganhnghe extends Model
{
    protected $table = 'nganhnghe';
    public function usercompany(){
    	return $this->hasMany('App\usercompany', 'Id_NganhNge','Id_UserCompany');
    }
    public function tintuyendung(){
    	return $this->hasManyThrough('App\tintuyendung','App\usercompany','Id_NganhNghe','Id_Company','Id_TinTuyenDung');
    }
}
